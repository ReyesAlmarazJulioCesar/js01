/*Modulo escuela*/
var Escuela=(function(){
    var _alumnos=[];
    var _docentes=[];
    var _grupos=[];
    var _materias=[];
    
    function _lista_de_alumnos(){
      return _alumnos;
    }
    
   
    function _agregar_alu(obj){
      return _alumnos.push(obj);
    }
    
    function _lista_de_doc(){
      return _docentes;
    }
    
   
    function _agregar_doc(obj){
      return _docentes.push(obj);
    }
    
    function _lista_de_grupos(){
      return _grupos;
    }
    
   
    function _agregar_grupo(obj){
      return _grupos.push(obj);
    }
    
    function _lista_de_materias(){
      return _materias;
    }
    
   
    function _agregar_materia(obj){
      return _materias.push(obj);
    }
    
    return{
      "listaalumnos":_lista_de_alumnos,
      "agregaralu":_agregar_alu,
      "agregar_doc":_agregar_doc,
      "listadocentes":_lista_de_doc,
      "agregar_grupo":_agregar_grupo,
      "listagrupo":_lista_de_grupos,
      "agregarmateria":_agregar_materia,
      "lista_dematerias":_lista_de_materias
    };
  })(); 
